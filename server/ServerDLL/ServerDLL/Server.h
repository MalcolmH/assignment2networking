#pragma once

#ifdef FUNCDLL_EXPORT
#define DLL_API __declspec(dllexport) 
#else
#define DLL_API __declspec(dllimport) 
#endif

#include <fstream>
#include "Utilities.h"

extern "C"
{
	DLL_API int Init();
	DLL_API int SendMsg(const char *msg, const int proto);
	DLL_API const char* UDPFunction();
	DLL_API const char* TCPListen();
	DLL_API const char* TCPReceive();
	DLL_API void Close();
}

class Server
{
public:
	Server();
	~Server();

};

