﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    private Text console;
    private int s = -1;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        console = GameObject.Find("Console").GetComponent<Text>();

        if (s == -1)
        {
            s = Server.Init();
            print(s);
            if (s == 0)
            {
                console.text = "Server Hosted!";
            }
            else
            {
                console.text = "Error! " + s;
            }
        }
    }
    
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (s == 0)
        {
            string udpFunc = Marshal.PtrToStringAnsi(Server.UDPFunction());
            string tcpListen = Marshal.PtrToStringAnsi(Server.TCPListen());
            string tcpRecv = Marshal.PtrToStringAnsi(Server.TCPReceive());

            if(udpFunc == "P1Start")
            {
                SceneManager.LoadScene(1);
                s = -99;
            }

            console.text += udpFunc;
            console.text += tcpListen;
            console.text += tcpRecv;
        }
        else if (s == -99)
        {
            string tcpRecv = Marshal.PtrToStringAnsi(Server.TCPReceive());
            if(tcpRecv != "")
            console.text = tcpRecv;
        }
    }

    void OnApplicationQuit ()
    {
        Server.Close();
    }
}
