﻿using System;
using System.Runtime.InteropServices;

public class Server
{
    [DllImport("ServerDLL")]
    public static extern int Init();

    [DllImport("ServerDLL")]
    public static extern int SendMsg([MarshalAs(UnmanagedType.LPStr)]string msg, int proto);

    [DllImport("ServerDLL")]
    public static extern IntPtr UDPFunction();

    [DllImport("ServerDLL")]
    public static extern IntPtr TCPListen();

    [DllImport("ServerDLL")]
    public static extern IntPtr TCPReceive();

    [DllImport("ServerDLL")]
    public static extern void Close();
}