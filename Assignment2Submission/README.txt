Assignment 2 - Pong with Dead Reckoning

By: - Malcolm Rose Harriott 	100568964
    - Felicia Albertina 	100526516

Controls:
- Move paddle with arrow keys!
- Click on text boxes to interact with them and press enter to confirm!
- Control the latency by the slider beside chat box

Known Error:
- score is not synced between clients
- left paddle does not always update, we think it might be because it is using the same
socket to send so for some reason the game only sends the right player. We did not have
time to fix it before submission

NOTE: Networking was done using C++ DLLs, find in respective folders.