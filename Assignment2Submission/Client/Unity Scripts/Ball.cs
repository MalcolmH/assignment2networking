﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

public class Ball : MonoBehaviour
{
    private Vector3 velocity;
    private int scoreL, scoreR = 0;
    public Text scoreLText, scoreRText, winText;
    private float speed = 5f;

    // Use this for initialization
    void Start ()
    {
        velocity = Vector3.Normalize(new Vector3(speed, speed, 0f)) * speed;
	}
    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }

    // Update is called once per frame
    void Update ()
    {
        transform.position += velocity * Time.deltaTime;
        if (scoreL > 10)
        {
            velocity = Vector3.zero;
            transform.position = new Vector3(0f, 1f, 1f);

            StartCoroutine(WinLeft());
        }
        else if (scoreR > 10)
        {
            velocity = Vector3.zero;
            transform.position = new Vector3(0f, 1f, 1f);

            StartCoroutine(WinRight());
        }

        /*string recvUdp = Marshal.PtrToStringAnsi(Client.RecvMsg(1));
        if (recvUdp.StartsWith("B"))
        {
            recvUdp = recvUdp.Substring(1);
            velocity = StringToVector3(recvUdp);
        }*/
    }

    IEnumerator WinRight ()
    {
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        /*speed = 5f;
        scoreL = 0;
        scoreLText.text = scoreL.ToString();
        scoreR = 0;
        scoreRText.text = scoreR.ToString();
        velocity = Vector3.Normalize(new Vector3(speed, speed, 0f)) * speed;*/
    }
    IEnumerator WinLeft()
    {
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        /*speed = 5f;
        scoreL = 0;
        scoreLText.text = scoreL.ToString();
        scoreR = 0;
        scoreRText.text = scoreR.ToString();
        velocity = Vector3.Normalize(new Vector3(speed, speed, 0f)) * speed;*/
    }

    void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.name == "WallT" || other.gameObject.name == "WallB")
        {
            velocity.y = -velocity.y;
        }
        else if (other.gameObject.name == "WallL")
        {
            scoreR += 1;
            scoreRText.text = scoreR.ToString();
            speed = 5f;
            velocity = Vector3.Normalize(new Vector3(speed, 0f, 0f)) * speed;
            transform.position = new Vector3(-6.5f, GameObject.Find("PaddleL").transform.position.y, 0f);
        }
        else if (other.gameObject.name == "WallR")
        {
            scoreL += 1;
            scoreLText.text = scoreL.ToString();
            speed = 5f;
            velocity = Vector3.Normalize(new Vector3(-speed, 0f, 0f)) * speed;
            transform.position = new Vector3(6.5f, GameObject.Find("PaddleR").transform.position.y, 0f);
        }
        else if (other.gameObject.name == "PaddleL" || other.gameObject.name == "PaddleR")
        {
            //speed += 1f;
            velocity.x = -velocity.x;
            velocity.y = (transform.position.y - other.transform.position.y) * speed;
            //while (velocity.y == 0f)
            //{
              //  velocity.y = Random.Range(-0.1f, 0.1f);
            //}

            velocity = Vector3.Normalize(velocity) * speed;
        }
    }
}
