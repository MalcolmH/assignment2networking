#pragma once
#include <string>
#include <vector>

#ifndef M_PI
#define M_PI 3.14159265359
#endif

std::string ltrim(const std::string &s);

std::string rtrim(const std::string &s);

std::string trim(const std::string &s);

std::vector<std::string> split(const std::string &s, const std::string &delimiters, bool shouldTrim = true);

std::string toLower(const std::string &s);

void seedRandomNumberGenerator();
void seedRandomNumberGenerator(unsigned int seed);

float randomRangef(float min, float max);
int randomRangei(int min, int max);