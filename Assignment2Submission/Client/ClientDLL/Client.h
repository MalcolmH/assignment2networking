#pragma once

#ifdef FUNCDLL_EXPORT
#define DLL_API __declspec(dllexport) 
#else
#define DLL_API __declspec(dllimport) 
#endif

#include <fstream>
#include "Utilities.h"

extern "C"
{
	DLL_API int Init(const char *addr);
	DLL_API int SendMsg(const char *msg, const int proto);
	DLL_API const char* RecvMsg(const int proto);
	DLL_API void Close();
}

class Client
{
public:
	Client();
	~Client();

};

