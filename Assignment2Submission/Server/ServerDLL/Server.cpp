#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>
#include "Server.h"

#pragma comment(lib, "ws2_32.lib")

// Initialize variables
SOCKET udpSock, udpSock2, tcpSock;
SOCKADDR_IN udpAddr, udpAddr2, tcpAddr;

std::vector<SOCKADDR_IN> udpClients;
std::vector<SOCKET> tcpClients;
std::vector<SOCKADDR_IN> tcpAddresses;

bool running = true;
int err = 0;

extern "C"
{
	int Init()
	{
		// Initialize Winsock
		WSADATA wsaData;

		// Check if initalize error
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		// Create socket
		udpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		udpSock2 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		tcpSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		// Check if socket creation error
		if (udpSock == INVALID_SOCKET || udpSock2 == INVALID_SOCKET || tcpSock == INVALID_SOCKET)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		// Socket settings
		udpAddr.sin_port = htons(8888);
		udpAddr.sin_family = AF_INET;
		udpAddr.sin_addr.s_addr = INADDR_ANY;

		udpAddr2.sin_port = htons(8889);
		udpAddr2.sin_family = AF_INET;
		udpAddr2.sin_addr.s_addr = INADDR_ANY;

		tcpAddr.sin_port = htons(8890);
		tcpAddr.sin_family = AF_INET;
		tcpAddr.sin_addr.s_addr = INADDR_ANY;

		// Set blocking mode
		u_long mode = 1; // non-blocking
		ioctlsocket(udpSock, FIONBIO, &mode);
		ioctlsocket(udpSock2, FIONBIO, &mode);
		ioctlsocket(tcpSock, FIONBIO, &mode);

		// Check if bind error
		if (bind(udpSock, (struct sockaddr *) &udpAddr, sizeof(udpAddr)) == SOCKET_ERROR || bind(udpSock2, (struct sockaddr *) &udpAddr2, sizeof(udpAddr2)) == SOCKET_ERROR || bind(tcpSock, (struct sockaddr *) &tcpAddr, sizeof(tcpAddr)) == SOCKET_ERROR)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		// Start running
		running = true;
		
		return err;
	}

	int SendMsg(const char *msg, const int proto)
	{
		if (running)
		{
			// Send via TCP
			if (proto < 1)
			{
				// Iterate over TCP clients
				for (int i = 0; i < tcpClients.size(); i++)
				{
					for (SOCKET c2 : tcpClients)
					{
						if (c2 != tcpClients[i])
						{
							// Send message
							send(c2, msg, strlen(msg), 0);

							err = WSAGetLastError();
							// Check if send error
							if (err != WSAEWOULDBLOCK && err != 0)
							{
								err = WSAGetLastError();
								running = false;
								return err;
							}
						}
					}
				}
			}
			// Send via UDP
			else if (proto < 2 && proto >= 1)
			{
				// Iterate over UDP clients
				for (int i = 0; i < udpClients.size(); i++)
				{
					// Send message, and check if send error
					if (sendto(udpSock, msg, strlen(msg), 0, (struct sockaddr *) &udpClients[i], sizeof(udpClients[i])) == SOCKET_ERROR)
					{
						err = WSAGetLastError();
						running = false;
						return err;
					}
				}
			}
			// Send via UDP socket 2
			else
			{
				// Iterate over UDP clients
				for (int i = 0; i < udpClients.size(); i++)
				{
					// Send message, and check if send error
					if (sendto(udpSock2, msg, strlen(msg), 0, (struct sockaddr *) &udpClients[i], sizeof(udpClients[i])) == SOCKET_ERROR)
					{
						running = false;
						return err;
					}
				}
			}
		}
		else
		{
			// Stop running
			return -3;
		}

		return err;
	}
	
	const char* TCPListen()
	{
		// Create buffer to store message
		char buffer[1024];

		// Listen for client
		listen(tcpSock, 1);
		SOCKET clientSock;

		if (running)
		{
			// Accept client
			SOCKADDR_IN clientAddr;
			clientSock = SOCKET_ERROR;
			clientSock = accept(tcpSock, (struct sockaddr *) &clientAddr, NULL);

			if (clientSock != SOCKET_ERROR)
			{
				// If no connection error, send connection message
				std::stringstream ss;
				ss << std::endl << "Client connected via TCP: " << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << std::endl;
				std::string msg = ss.str();

				strcpy_s(buffer, msg.length() + 1, msg.c_str());

				// Set blocking mode
				u_long mode = 1;
				ioctlsocket(clientSock, FIONBIO, &mode);

				// Add client that connected to TCP clients vector
				tcpClients.push_back(clientSock);
				tcpAddresses.push_back(clientAddr);

				// Return the message
				return buffer;
			}

		}

		return "";
	}

	const char* TCPReceive()
	{
		// Create buffer to store message
		char buffer[1024];
		std::string msg;

		if (running)
		{
			// Iterate over clients
			for (int i = 0; i < tcpClients.size(); i++)
			{
				memset(buffer, 0, 1024); // Clear buffer

				// Receive message data from the client, i
				int dataLength = recv(tcpClients[i], buffer, 1024, 0);
				if (dataLength == -1)
				{
					continue;
				}

				// Create message string with client number
				std::stringstream ss;
				ss << "[Player " << i << "]: " << buffer << std::endl;
				msg = ss.str();


				err = WSAGetLastError();
				// Check if receive error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
					msg = "Recv failed with error code: " + err;
					strcpy_s(buffer, msg.length() + 1, msg.c_str());
					return buffer;
				}

				// Broadcast message to all clients
				for (SOCKET c2 : tcpClients)
				{
					if (c2 != tcpClients[i])
					{
						// Send message
						send(c2, msg.c_str(), msg.length(), 0);

						err = WSAGetLastError();
						// Check if send error
						if (err != WSAEWOULDBLOCK && err != 0)
						{
							err = WSAGetLastError();
							running = false;
							msg = "Send failed with error code: " + err;
							strcpy_s(buffer, msg.length() + 1, msg.c_str());
							return buffer;
						}
					}
				}

			}
			strcpy_s(buffer, msg.length() + 1, msg.c_str());
			return buffer;

		}

		return "";
	}

	const char* UDPFunction()
	{
		// Create buffer to store message
		char buffer[1024];
		std::string msg;

		SOCKADDR_IN senderAddr;

		if (running)
		{
			memset(buffer, 0, 1024); // Clear buffer
			int slen = sizeof(udpAddr);
			int recvlen;

			// Receive message data from the UDP socket
			recvlen = recvfrom(udpSock, buffer, 1024, 0, (struct sockaddr *) &senderAddr, &slen);

			err = WSAGetLastError();
			// Check if receive error
			if (err != WSAEWOULDBLOCK && err != 0)
			{
				err = WSAGetLastError();
				msg = "Recvfrom() failed with error code: " + err;
				strcpy_s(buffer, msg.length() + 1, msg.c_str());
				return buffer;
			}
			else if (err == 0)
			{
				// Check if buffer starts with '0', means new client connection
				if (buffer[0] == '0')
				{
					// New connection request
					udpClients.push_back(senderAddr);

					// Send connection message
					std::stringstream ss;
					ss << std::endl << "Client " << inet_ntoa(senderAddr.sin_addr) << ":" << ntohs(senderAddr.sin_port) << " has joined the room.";
					msg = ss.str();

					// Broadcast connection message to other clients
					for (int i = 0; i < udpClients.size() - 1; i++)
					{
						// Check if send error
						if (sendto(udpSock, msg.c_str(), msg.length(), 0, (struct sockaddr *) &udpClients[i], sizeof(udpClients[i])) == SOCKET_ERROR)
						{
							msg = "Sendto() failed with error code: " + err;
							strcpy_s(buffer, msg.length() + 1, msg.c_str());
							return buffer;
						}
					}
					strcpy_s(buffer, msg.length() + 1, msg.c_str());
					return buffer;
				}
				// Check if buffer starts with '1', means new message
				else if (buffer[0] == '1')
				{
					std::string msg(buffer);
					msg = msg.substr(1, recvlen - 1); // Remove the '1' at the start

					// Iterate over the clients
					for (int i = 0; i < udpClients.size(); i++)
					{
						if (udpClients[i].sin_addr.s_addr == senderAddr.sin_addr.s_addr && udpClients[i].sin_port == senderAddr.sin_port)
						{
							msg = std::to_string(i) + msg;
						}
					}

					// Message received, broadcast to all other clients
					for (int i = 0; i < udpClients.size(); i++)
					{
						if (udpClients[i].sin_addr.s_addr != senderAddr.sin_addr.s_addr || udpClients[i].sin_port != senderAddr.sin_port)
						{
							// Check if send error
							if (sendto(udpSock, msg.c_str(), msg.length(), 0, (struct sockaddr *) &udpClients[i], sizeof(udpClients[i])) == SOCKET_ERROR)
							{
								err = WSAGetLastError();
								running = false;
								msg = "Sendto() failed with error code: " + err;
								strcpy_s(buffer, msg.length() + 1, msg.c_str());
								return buffer;
							}
						}
					}
					strcpy_s(buffer, msg.length() + 1, msg.c_str());
					return buffer;
				}
			}

		}

		return "";
	}

	void Close()
	{
		// Close sockets and shutdown if running
		if (running)
		{
			shutdown(udpSock, SD_SEND);
			closesocket(udpSock);

			shutdown(udpSock2, SD_SEND);
			closesocket(udpSock2);

			shutdown(tcpSock, SD_SEND);
			closesocket(tcpSock);

			for (SOCKET c : tcpClients)
			{
				shutdown(c, SD_SEND);
				closesocket(c);
			}

			WSACleanup();
			running = false;
			Sleep(100);
		}
	}
}

Server::Server()
{
}


Server::~Server()
{
}
