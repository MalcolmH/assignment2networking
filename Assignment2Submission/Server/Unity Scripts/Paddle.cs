﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Paddle : MonoBehaviour
{
    private float speed = 15f;
    GameObject rightPaddle;
    GameObject leftPaddle;
    DeadReckoning deadReckoning;

    string recvUdp;

    float timer;

    bool converge = false;
    float startTime, journeyLength;

    Vector3 pPos, oPos;
    // Use this for initialization
    private void Awake()
    {
        deadReckoning = GetComponent<DeadReckoning>();
    }

    // convert the packets sent from the clients (paddle position) into vector3
    public static Vector3 StringToVector3(ref string sVector)
    {

        int pFrom = sVector.IndexOf("(") + 1;
        int pTo = sVector.LastIndexOf(")");

        string res = sVector.Substring(pFrom, pTo - pFrom);

        // split the items
        string[] sArray = res.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        sVector = sVector.Substring(sVector.IndexOf(")") + 1);
        return result;
    }

    // Update is called once per frame
    void Update()
    {       
        recvUdp = Marshal.PtrToStringAnsi(Server.UDPFunction());
        print(recvUdp);
            if (name == "PaddleL")
            {
                recvUdp = recvUdp.Substring(1);
                if (recvUdp.Contains("(") && recvUdp.Contains(")"))
                {
                    pPos = StringToVector3(ref recvUdp);
                    speed = float.Parse(recvUdp);
                    print("P1 " + pPos);
                    print("P1 " + speed);
                    converge = true;
                    startTime = Time.time;
                    oPos = transform.position;
                    journeyLength = Vector3.Distance(oPos, pPos);
                }
            }
            else if (name == "PaddleR" && recvUdp.StartsWith("1"))
            {
                recvUdp = recvUdp.Substring(1);
                if (recvUdp.Contains("(") && recvUdp.Contains(")"))
                {
                    pPos = StringToVector3(ref recvUdp);
                    speed = float.Parse(recvUdp);
                    print("P2 " + pPos);
                    print("P2 " + speed);
                    converge = true;
                    startTime = Time.time;
                    oPos = transform.position;
                    journeyLength = Vector3.Distance(oPos, pPos);
                }
            }

            // performs dead reckoning to the paddles on server side
        if (!converge)
        {
            if (name == "PaddleR" )
            {
                transform.position = new Vector3(transform.position.x, deadReckoning.nextPosition(transform.position.y, speed, Time.deltaTime), transform.position.z);
            }
            else if (name == "PaddleL" )
            {
                transform.position = new Vector3(transform.position.x, deadReckoning.nextPosition(transform.position.y, speed, Time.deltaTime), transform.position.z);
            }
        }
        else
        {
            if (name == "PaddleR" )
            {
                float distCovered = (Time.time - startTime) * 15f;
                float fracJourney = distCovered / journeyLength;
                // - error convergence - perform smooth transition for the paddles 
                transform.position = Vector3.Lerp(oPos, pPos, fracJourney);
            }
            else if (name == "PaddleL")
            {
                float distCovered = (Time.time - startTime) * 15f;
                float fracJourney = distCovered / journeyLength;
                // - error convergence - perform smooth transition for the paddles 
                transform.position = Vector3.Lerp(oPos, pPos, fracJourney);
            }
        }
       
    }
}
