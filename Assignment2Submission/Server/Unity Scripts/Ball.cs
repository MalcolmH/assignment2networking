﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    private Vector3 velocity;
    private int scoreL, scoreR = 0;
    public Text scoreLText, scoreRText, winText;
    private float speed = 5f;

    // Use this for initialization
    void Start ()
    {
        velocity = Vector3.Normalize(new Vector3(speed, speed, 0f)) * speed;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position += velocity * Time.deltaTime;

        // checks which player scores 10 first, then restart game
        if (scoreL > 10)
        {
            velocity = Vector3.zero;
            transform.position = new Vector3(0f, 1f, 1f);

            StartCoroutine(WinLeft());
        }
        else if (scoreR > 10)
        {
            velocity = Vector3.zero;
            transform.position = new Vector3(0f, 1f, 1f);

            StartCoroutine(WinRight());
        }
    }

    IEnumerator WinRight ()
    {
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Right Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
    IEnumerator WinLeft()
    {
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = "Left Player Wins!";
        yield return new WaitForSeconds(2f * Time.deltaTime);
        winText.text = " ";
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.name == "WallT" || other.gameObject.name == "WallB")
        {
            velocity.y = -velocity.y;
        }
        else if (other.gameObject.name == "WallL")
        {
            scoreR += 1;
            scoreRText.text = scoreR.ToString();
            speed = 5f;
            velocity = Vector3.Normalize(new Vector3(speed, 0f, 0f)) * speed;
            transform.position = new Vector3(-6.5f, GameObject.Find("PaddleL").transform.position.y, 0f);
        }
        else if (other.gameObject.name == "WallR")
        {
            scoreL += 1;
            scoreLText.text = scoreL.ToString();
            speed = 5f;
            velocity = Vector3.Normalize(new Vector3(-speed, 0f, 0f)) * speed;
            transform.position = new Vector3(6.5f, GameObject.Find("PaddleR").transform.position.y, 0f);
        }
        else if (other.gameObject.name == "PaddleL" || other.gameObject.name == "PaddleR")
        {
            velocity.x = -velocity.x;
            velocity.y = (transform.position.y - other.transform.position.y) * speed;

            velocity = Vector3.Normalize(velocity) * speed;
        }
    }
}
