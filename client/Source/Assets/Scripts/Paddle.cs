﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class Paddle : MonoBehaviour
{
    private float speed = 0f;

    public float time = 0.5f;

    Vector3 pPos, oPos;

    DeadReckoning deadReckoning;
    public Slider slider;
    public Text LatencyText;

    public GUISkin guiSkin;

    string recvUdp;

    float timer;

    bool converge = false;
    float startTime, journeyLength;

    void Start ()
    {
        deadReckoning = GetComponent<DeadReckoning>();

        pPos = gameObject.transform.position;
        sendPaddlePosition();

        InvokeRepeating("updatePosition", 0.05f, time);
        InvokeRepeating("sendPaddlePosition", 0f, time);

        InvokeRepeating("sliderCheck", 0, 5);

        // create a slider so users can change the latency through the slider
        time = slider.value;

        slider.value = 0.5f;

        slider.maxValue = 10;
    }
    
    void sliderCheck()
    {
        Debug.Log("time " + time);
    }

    public static Vector3 StringToVector3(ref string sVector)
    {

            int pFrom = sVector.IndexOf("(") + 1;
            int pTo = sVector.LastIndexOf(")");

            string res = sVector.Substring(pFrom, pTo - pFrom);

            // split the items
            string[] sArray = res.Split(',');

            // store as a Vector3
            Vector3 result = new Vector3(
                float.Parse(sArray[0]),
                float.Parse(sArray[1]),
                float.Parse(sArray[2]));

            sVector = sVector.Substring(sVector.IndexOf(")") + 1);
            return result;
    }

    // Update is called once per frame
    void Update()
    {

        // create a slider so users can change the latency through the slider
        time = slider.value;

        LatencyText.text = time.ToString();

        if (name == "PaddleR" && Input.GetKey(KeyCode.UpArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            speed = 15f;
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleR" && Input.GetKey(KeyCode.DownArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            speed = -15f;
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleL" && Input.GetKey(KeyCode.UpArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            speed = 15f;
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleL" && Input.GetKey(KeyCode.DownArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            speed = -15f;
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if ((name == "PaddleL" && GameObject.Find("GameController").GetComponent<Controller>().player == 1) || (name == "PaddleR" && GameObject.Find("GameController").GetComponent<Controller>().player == 2))
            speed = 0f;

        if (transform.position.y > 5.5f)
        {
            transform.position = new Vector3(transform.position.x, 5.5f, transform.position.z);
        }
        else if(transform.position.y < -3.5f)
        {
            transform.position = new Vector3(transform.position.x, -3.5f, transform.position.z);
        }

        // perform dead reckoning
        predictPosition(); //deadReckoning

    }

    void predictPosition()
    {
        if (!converge)
        {
            if (name == "PaddleR" && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
            {
                transform.position = new Vector3(transform.position.x, deadReckoning.nextPosition(transform.position.y, speed, Time.deltaTime), transform.position.z);
            }
            else if (name == "PaddleL" && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
            {
                transform.position = new Vector3(transform.position.x, deadReckoning.nextPosition(transform.position.y, speed, Time.deltaTime), transform.position.z);
            }
        }
        else
        {
            if (name == "PaddleR" && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
            {
                float distCovered = (Time.time - startTime) * 15f;
                float fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(oPos, pPos, fracJourney);
            }
            else if (name == "PaddleL" && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
            {
                float distCovered = (Time.time - startTime) * 15f;
                float fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(oPos, pPos, fracJourney);
            }
        }
    }

    void updatePosition() 
    {
        recvUdp = Marshal.PtrToStringAnsi(Client.RecvMsg(1));
        if (name == "PaddleL" && recvUdp.StartsWith("P0"))// && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            if (recvUdp.Contains("(") && recvUdp.Contains(")"))
            {
                print(recvUdp);
                recvUdp = recvUdp.Substring(2);
                pPos = StringToVector3(ref recvUdp);
                speed = float.Parse(recvUdp);
                //print("P1 "+pPos);
                //print("P1 "+speed);
                converge = true;
                startTime = Time.time;
                oPos = transform.position;
                journeyLength = Vector3.Distance(oPos, pPos);
            }
        }
        else if (name== "PaddleR" && recvUdp.StartsWith("P1"))// && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            if (recvUdp.Contains("(") && recvUdp.Contains(")"))
            {
                print(recvUdp);
                recvUdp = recvUdp.Substring(2);
                pPos = StringToVector3(ref recvUdp);
                speed = float.Parse(recvUdp);
                //print("P2 "+pPos);
                //print("P2 "+speed);
                converge = true;
                startTime = Time.time;
                oPos = transform.position;
                journeyLength = Vector3.Distance(oPos, pPos);
            }
        }

    }

    void sendPaddlePosition() // send paddle position
    {
        if (name == "PaddleL" && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
            Client.SendMsg(transform.position.ToString() + speed.ToString(), 1);

        else if (name == "PaddleR" && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
            Client.SendMsg(transform.position.ToString() + speed.ToString(), 1);
    }
}
