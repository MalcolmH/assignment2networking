﻿using System;
using System.Runtime.InteropServices;

public class Client
{
    [DllImport("ClientDLL")]
    public static extern int Init([MarshalAs(UnmanagedType.LPStr)]string addr);

    [DllImport("ClientDLL")]
    public static extern int SendMsg([MarshalAs(UnmanagedType.LPStr)]string msg, int proto);

    [DllImport("ClientDLL")]
    public static extern IntPtr RecvMsg(int proto);

    [DllImport("ClientDLL")]
    public static extern void Close();
}