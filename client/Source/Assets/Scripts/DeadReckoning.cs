﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DeadReckoning : MonoBehaviour
{
    public float nextPosition(float previousPosition, float velocity, float time)
    {
        float nextPos = previousPosition + (velocity * time);
        return nextPos;
    }
}
