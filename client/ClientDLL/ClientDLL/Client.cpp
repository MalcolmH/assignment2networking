#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <WinSock2.h>
#include <thread>
#include <chrono>
#include <mutex>
#include "Client.h"

#pragma comment(lib, "ws2_32.lib")

SOCKET udpSock, udpSock2, tcpSock;
SOCKADDR_IN udpAddr, udpAddr2, tcpAddr;
int slen, slen2;
int err = 0;
bool running = true;
//std::mutex mtx;

extern "C"
{
	int Init(const char *addr)
	{
		// Initialize Winsock
		WSADATA wsaData;

		// Check if initalize error
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			WSACleanup();
			return -1;
		}

		// Create socket
		udpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		udpSock2 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		tcpSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		// Check if socket creation error
		if (udpSock == INVALID_SOCKET || udpSock2 == INVALID_SOCKET || tcpSock == INVALID_SOCKET)
		{
			WSACleanup();
			return -2;
		}

		// Socket settings
		udpAddr.sin_port = htons(8888);
		udpAddr.sin_family = AF_INET;
		udpAddr.sin_addr.s_addr = inet_addr(addr);
		slen = sizeof(udpAddr);

		udpAddr2.sin_port = htons(8889);
		udpAddr2.sin_family = AF_INET;
		udpAddr2.sin_addr.s_addr = inet_addr(addr);
		slen2 = sizeof(udpAddr2);

		tcpAddr.sin_port = htons(8890);
		tcpAddr.sin_family = AF_INET;
		tcpAddr.sin_addr.s_addr = inet_addr(addr);

		// Connect via TCP and check if error
		if (connect(tcpSock, (struct sockaddr *) &tcpAddr, sizeof(tcpAddr)) != 0)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		// Set blocking mode
		u_long mode = 1; // non-blocking
		ioctlsocket(udpSock, FIONBIO, &mode);
		ioctlsocket(udpSock2, FIONBIO, &mode);
		ioctlsocket(tcpSock, FIONBIO, &mode);

		// Start with '0' to show that it is a new client to server
		std::string connectMsg = "0";
		sendto(udpSock2, connectMsg.c_str(), connectMsg.length(), 0, (struct sockaddr *) &udpAddr2, slen2);

		err = WSAGetLastError();
		// Check if send error
		if (err != WSAEWOULDBLOCK && err != 0)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		sendto(udpSock, connectMsg.c_str(), connectMsg.length(), 0, (struct sockaddr *) &udpAddr, slen);

		err = WSAGetLastError();
		// Check if send error
		if (err != WSAEWOULDBLOCK && err != 0)
		{
			err = WSAGetLastError();
			WSACleanup();
			return err;
		}

		// Start running
		running = true;

		return err;
	}

	int SendMsg(const char *msg, const int proto)
	{
		if (running)
		{
			std::string msgStr = msg;

			// Send via TCP
			if (proto < 1)
			{
				send(tcpSock, msgStr.c_str(), msgStr.length(), 0);

				err = WSAGetLastError();
				// Check if send error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
				}
			}
			// Send via UDP
			else
			{
				// Start with '1' to show new message, then send
				msgStr = "1" + msgStr;
				sendto(udpSock, msgStr.c_str(), msgStr.length(), 0, (struct sockaddr *) &udpAddr, slen);

				err = WSAGetLastError();
				// Check if send error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
				}
			}
		}
		else
		{
			// Stop running
			return -3;
		}

		return err;
	}

	const char* RecvMsg(const int proto)
	{
		// Create buffer for message
		char buffer[1024];
		if (running)
		{
			// Receive via TCP socket
			if (proto < 1)
			{
				memset(buffer, 0, 1024); // Clear buffer

										 // Receive message
				int length = recv(tcpSock, buffer, 1024, 0);

				err = WSAGetLastError();
				// Check if receive error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
				}
				// Otherwise return the message
				else if (err == 0 && strlen(buffer) > 0)
				{
					return buffer;
				}

			}
			// Receive via UDP socket
			else if (proto < 2 && proto >= 1)
			{
				memset(buffer, 0, 1024); // Clear buffer

										 // Receive message
				recvfrom(udpSock, buffer, 1024, 0, (struct sockaddr *) &udpAddr, &slen);

				err = WSAGetLastError();
				// Check if receive error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
				}
				// Otherwise return the message
				else if (err == 0)
				{
					//if (buffer[0] == 'P')
						return buffer;
				}
			}
			// Receive via UDP socket 2
			else
			{
				memset(buffer, 0, 1024); // Clear buffer

										 // Receive message
				int recvlen = recvfrom(udpSock2, buffer, 1024, 0, (struct sockaddr *) &udpAddr2, &slen2);

				err = WSAGetLastError();
				// Check if receive error
				if (err != WSAEWOULDBLOCK && err != 0)
				{
					err = WSAGetLastError();
					running = false;
				}
				// Otherwise return the message
				else if (err == 0)
				{
					if (buffer[0] == '3')
					{
						std::string msg(buffer);
						msg = msg.substr(1, recvlen - 1); // Remove the '3' at the start
						strcpy_s(buffer, msg.length() + 1, msg.c_str());
						return buffer;
					}
				}
			}
		}
		return "";
	}

	void Close()
	{
		// Close sockets and shutdown
		if (running)
		{
			shutdown(udpSock, SD_SEND);
			closesocket(udpSock);

			shutdown(udpSock2, SD_SEND);
			closesocket(udpSock2);

			shutdown(tcpSock, SD_SEND);
			closesocket(tcpSock);

			WSACleanup();
			running = false;
		}

	}

}

Client::Client()
{
}


Client::~Client()
{
}
