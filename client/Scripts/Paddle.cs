﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Paddle : MonoBehaviour
{
    private float speed = 15f;

	// Use this for initialization
	void Start ()
    {
		
	}
    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }

    // Update is called once per frame
    void Update()
    {
        string recvUdp = Marshal.PtrToStringAnsi(Client.RecvMsg(1));
        if (name == "PaddleR" && recvUdp.StartsWith("1") && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            recvUdp = recvUdp.Substring(1);
            transform.position = StringToVector3(recvUdp);
        }
        else if (name == "PaddleL" && recvUdp.StartsWith("0") && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            recvUdp = recvUdp.Substring(1);
            transform.position = StringToVector3(recvUdp);
        }
        
        //string recvUdp = Marshal.PtrToStringAnsi(Client.RecvMsg(1));
        if (name == "PaddleR" && Input.GetKey(KeyCode.UpArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleR" && Input.GetKey(KeyCode.DownArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
        {
            transform.position += new Vector3(0f, -speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleL" && Input.GetKey(KeyCode.UpArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            transform.position += new Vector3(0f, speed, 0f) * Time.deltaTime;
        }
        else if (name == "PaddleL" && Input.GetKey(KeyCode.DownArrow) && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
        {
            transform.position += new Vector3(0f, -speed, 0f) * Time.deltaTime;
        }

        if (transform.position.y > 5.5f)
        {
            transform.position = new Vector3(transform.position.x, 5.5f, transform.position.z);
        }
        else if(transform.position.y < -3.5f)
        {
            transform.position = new Vector3(transform.position.x, -3.5f, transform.position.z);
        }

        if (name == "PaddleL" && GameObject.Find("GameController").GetComponent<Controller>().player == 1)
            Client.SendMsg(transform.position.ToString(), 1);
        else if (name == "PaddleR" && GameObject.Find("GameController").GetComponent<Controller>().player == 2)
            Client.SendMsg(transform.position.ToString(), 1);
    }
}
