﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;


public class Controller : MonoBehaviour
{
    public int player = 2;
    private int s = -1;
    private Text console;
    private InputField input;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        console = GameObject.Find("Console").GetComponent<Text>();
        if (s == -9)
        {
            s = -99;
            print(Client.SendMsg("Start", 1));
            input = GameObject.Find("ChatField").GetComponent<InputField>();
            input.onEndEdit.AddListener(SubmitChat);
        }

    }

    private void SubmitChat(string chat)
    {
        console.text = chat;
        Client.SendMsg(chat, 0);
    }

    public void GetInput (string addr)
    {
        s = Client.Init(addr);
        print(s);
        console.text = "Connected!\n";
    }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (s == 0)
        {
            string recvUdp = Marshal.PtrToStringAnsi(Client.RecvMsg(1));
            string recvTcp = Marshal.PtrToStringAnsi(Client.RecvMsg(0));

            if(recvUdp == "0Ready")
            {
                s = -9;
                player = 2;
                SceneManager.LoadScene(1);
            }
            else if (recvUdp == "1Ready")
            {
                s = -9;
                player = 1;
                SceneManager.LoadScene(1);
            }

            if (recvUdp == "0Start")
            {
                //player = 2;
                s = -99;
            }
            else if(recvUdp == "1Start")
            {
                //player = 1;
                s = -99;
            }

            console.text += recvTcp;
            console.text += recvUdp;

            if (console.text != "Connected!\n")
            {
                //player = 1;
                print(Client.SendMsg("Ready", 1));
            }
        }
        else if (s == -99)
        {
            string recvTcp = Marshal.PtrToStringAnsi(Client.RecvMsg(0));
            if (recvTcp != "")
            {
                console.text = recvTcp;
                //print(recvTcp);
            }/*
            if (Input.GetKey(KeyCode.UpArrow))
            {
                print(Client.SendMsg("Up", 1));
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                print(Client.SendMsg("Down", 1));
            }*/
        }
    }

    void OnApplicationQuit()
    {
        Client.Close();
    }
}
